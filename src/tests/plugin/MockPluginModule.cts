import ts, { SourceFile } from "typescript";
import { LanguageService, server } from "typescript/lib/tsserverlibrary.js";

/**
 * Provides a plugin module for testing.
 */
export class MockPluginModule implements server.PluginModule
{
    /**
     * The error code of the diagnostic reported by this plugin.
     */
    public static readonly ErrorCode = 420;

    /**
     * The name of the error source reported by this plugin.
     */
    public static readonly SourceName = "mock";

    /**
     * A value indicating whether a diagnostic should be reported.
     */
    private report = false;

    /**
     * The TypeScript library.
     */
    private tsLibrary: typeof ts;

    /**
     * Initializes a new instance of the {@linkcode MockPluginModule} class.
     *
     * @param tsLibrary
     * The TypeScript library.
     */
    public constructor(tsLibrary: typeof ts)
    {
        this.tsLibrary = tsLibrary;
    }

    /**
     * Gets the TypeScript library.
     */
    public get TS(): typeof ts
    {
        return this.tsLibrary;
    }

    /**
     * Creates a decorated instance of the language service.
     *
     * @param createInfo
     * The info of the plugin creation.
     *
     * @returns
     * The decorated language service.
     */
    public create(createInfo: server.PluginCreateInfo): LanguageService
    {
        let languageService = createInfo.languageService;
        let proxy: LanguageService = Object.create(null);
        let diagnosticStart = (file: SourceFile): number => Math.min(file.getFullText().length / 2, 420);
        let length = (file: SourceFile): number => Math.min(file.getFullText().length / 2, 1337);

        for (let key of Object.keys(languageService) as Array<keyof LanguageService>)
        {
            const original = languageService[key] as (...args: any[]) => any;
            proxy[key] = (...args: any[]) => original.apply(languageService, args);
        }

        proxy.getSemanticDiagnostics = (fileName) =>
        {
            let result = languageService.getSemanticDiagnostics(fileName);

            if (this.report)
            {
                let file = languageService.getProgram()?.getSourceFile(fileName);

                if (file)
                {
                    result.push(
                        {
                            category: this.TS.DiagnosticCategory.Error,
                            code: MockPluginModule.ErrorCode,
                            source: MockPluginModule.SourceName,
                            file,
                            start: diagnosticStart(file),
                            length: length(file),
                            messageText: "mock error"
                        });
                }
            }

            return result;
        };

        proxy.getCodeFixesAtPosition = (fileName, start, end, errorCodes, formatOptions, preferences) =>
        {
            let file = languageService.getProgram()?.getSourceFile(fileName);
            let fixes = languageService.getCodeFixesAtPosition(fileName, start, end, errorCodes, formatOptions, preferences);

            if (
                file &&
                start === diagnosticStart(file) &&
                end === (diagnosticStart(file) + length(file)))
            {
                fixes = [
                    ...fixes,
                    {
                        fixName: "mock fix",
                        changes: [],
                        description: "mock"
                    }
                ];
            }

            return fixes;
        };

        return proxy;
    }

    /**
     * Handles a configuration update.
     *
     * @param config
     * The new configuration.
     */
    public onConfigurationChanged(config: any): void
    {
        this.report = config[nameof(() => this.report)];
    }
}
