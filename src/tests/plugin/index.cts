import { server } from "typescript/lib/tsserverlibrary.js";
import { MockPluginModule } from "./MockPluginModule.cjs";

/**
 * Creates a new plugin.
 *
 * @param info
 * The info of the plugin creation.
 *
 * @returns
 * The newly created plugin.
 */
let factory: (info: Parameters<server.PluginModuleFactory>[0]) => server.PluginModule = (info) =>
{
    return new MockPluginModule(info.typescript as any);
};

export = factory;
