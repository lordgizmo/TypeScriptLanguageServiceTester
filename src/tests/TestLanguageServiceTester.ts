import { TSConfigJSON } from "types-tsconfig";
import { MockWorkspace } from "./MockWorkspace.js";
import { TestConstants } from "./TestConstants.js";
import { LanguageServiceTester } from "../LanguageServiceTester.js";

/**
 * Provides an implementation of the {@linkcode LanguageServiceTester} class for testing.
 */
export class TestLanguageServiceTester extends LanguageServiceTester
{
    /**
     * Initializes a new instance of the {@linkcode TestLanguageServiceTester} class.
     *
     * @param workingDirectory
     * The working directory to set for the default workspace.
     */
    public constructor(workingDirectory = TestConstants.TestWorkspaceDirectory)
    {
        super(workingDirectory);
    }

    /**
     * @inheritdoc
     */
    public override get DefaultWorkspace(): MockWorkspace
    {
        return super.DefaultWorkspace as MockWorkspace;
    }

    /**
     * @inheritdoc
     */
    public override get TempWorkspaces(): readonly MockWorkspace[]
    {
        return super.TempWorkspaces as MockWorkspace[];
    }

    /**
     * Configures the default workspace.
     *
     * @param tsConfig
     * The TypeScript-settings to apply.
     */
    public override async Configure(tsConfig?: TSConfigJSON): Promise<void>
    {
        return this.DefaultWorkspace.Configure(tsConfig);
    }

    /**
     * @inheritdoc
     *
     * @param workspacePath
     * The path to the workspace to create.
     *
     * @returns
     * The newly created workspace.
     */
    protected override CreateWorkspace(workspacePath: string): MockWorkspace
    {
        return new MockWorkspace(this, workspacePath);
    }
}
