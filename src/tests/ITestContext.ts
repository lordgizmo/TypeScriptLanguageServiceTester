import { TestLanguageServiceTester } from "./TestLanguageServiceTester.js";

/**
 * Provides a test-context.
 */
export interface ITestContext
{
    /**
     * A component for testing tslint-diagnostics.
     */
    LanguageServiceTester: TestLanguageServiceTester;
}
