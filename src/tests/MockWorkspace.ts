import { join } from "path";
import { fileURLToPath } from "url";
import { Package, PackageType } from "@manuth/package-json-editor";
import fs from "fs-extra";
import merge from "lodash.merge";
import { TSConfigJSON } from "types-tsconfig";
import { TestWorkspace } from "../Workspaces/TestWorkspace.js";

const { writeJSON } = fs;

/**
 * Represents an tslint-workspace.
 */
export class MockWorkspace extends TestWorkspace
{
    /**
     * Gets the name of the typescript-plugin.
     */
    public get TypeScriptPluginName(): string
    {
        return "mock-plugin";
    }

    /**
     * @inheritdoc
     */
    public override get InstallerPackage(): Package
    {
        let result = super.InstallerPackage;

        let dependencies = [
            this.TypeScriptPluginName
        ];

        for (let dependency of dependencies)
        {
            if (!result.AllDependencies.Has(dependency))
            {
                result.DevelopmentDependencies.Add(
                    dependency,
                    `${this.PluginURL}`);
            }
        }

        return result;
    }

    /**
     * Gets the url to the mocked plugin.
     */
    protected get PluginURL(): URL
    {
        return new URL("plugin", import.meta.url);
    }

    /**
     * @inheritdoc
     */
    public override async Install(): Promise<void>
    {
        let pluginPackage = new Package(join(fileURLToPath(this.PluginURL), Package.FileName), {});
        pluginPackage.Main = "./index.cjs";
        pluginPackage.Type = PackageType.ESModule;

        await Promise.all(
            [
                super.Install(),
                writeJSON(
                    pluginPackage.FileName as string,
                    pluginPackage.ToJSON())
            ]);
    }

    /**
     * Configures the workspace.
     *
     * @param tsConfig
     * The TypeScript-settings to apply.
     */
    public override async Configure(tsConfig?: TSConfigJSON): Promise<void>
    {
        await super.Configure(
            merge<TSConfigJSON, TSConfigJSON>(
                {
                    compilerOptions: {
                        allowJs: true,
                        plugins: [
                            {
                                name: this.TypeScriptPluginName
                            }
                        ]
                    }
                },
                (tsConfig ?? {})));
    }
}
