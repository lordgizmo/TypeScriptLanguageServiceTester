import { DiagnosticTests } from "./Diagnostics/index.js";
import { ITestContext } from "./ITestContext.js";
import { LanguageServiceTesterTests } from "./LanguageServiceTester.test.js";
import { TestLanguageServiceTester } from "./TestLanguageServiceTester.js";
import { TSServerTests } from "./TSServer.test.js";
import { WorkspaceTests } from "./Workspaces/index.js";

suite(
    "TypeScriptLanguageServiceTester",
    () =>
    {
        let testContext: ITestContext = {
            LanguageServiceTester: undefined as any
        };

        suiteSetup(
            async function()
            {
                this.timeout(1.5 * 60 * 1000);
                testContext.LanguageServiceTester = new TestLanguageServiceTester();
                await testContext.LanguageServiceTester.Install();
            });

        suiteTeardown(
            async function()
            {
                this.timeout(10 * 1000);
                await testContext.LanguageServiceTester.Dispose();
            });

        TSServerTests();
        LanguageServiceTesterTests(testContext);
        WorkspaceTests(testContext);
        DiagnosticTests(testContext);
    });
